# coding: utf-8
import glfw
from OpenGL.GL import *
import ui.menuview
import ui.gameview
import ui.util
import nes.console


class Director:
    def __init__(self, window):
        self.window = window
        self.view = None
        self.menu_view = None
        self.timestamp = glfw.get_time()

    def set_title(self, title):
        glfw.set_window_title(self.window, title)

    def set_view(self, view):
        if self.view is not None:
            self.view.exit()
        self.view = view
        if self.view is not None:
            self.view.enter()
        self.timestamp = glfw.get_time()

    def step(self):
        glClear(GL_COLOR_BUFFER_BIT)
        timestamp = glfw.get_time()
        dt = timestamp - self.timestamp
        self.timestamp = timestamp
        if self.view is not None:
            self.view.update(dt)

    def start(self, paths):
        self.menu_view = ui.menuview.MenuView(self, paths)
        if len(paths) == 1:
            self.play_game(paths[0])
        else:
            self.show_menu()
        self.run()

    def run(self):
        while not glfw.window_should_close(self.window):
            self.step()
            glfw.swap_buffers(self.window)
            glfw.poll_events()
        self.set_view("")

    def play_game(self, path):
        hash_file = ui.util.hash_file(path)
        console = nes.console.Console.new_console(path)
        self.set_view(ui.gameview.GameView(self, console, path, hash_file))

    def show_menu(self):
        self.set_view(self.menu_view)
