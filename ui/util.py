# coding: utf-8

import os
import glfw
from OpenGL.GL import *
import hashlib
import nes.controller
from PIL import Image

home_dir = os.environ['HOME']


def hash_file(path):
    with open(path, 'rb') as f:
        data = f.read()
    return hashlib.md5(data).hexdigest()


def sram_path(hash_str):
    return home_dir + "./nes/sram/" + hash_str + ".dat"


def save_path(hash_str):
    return home_dir + "/.nes/save/" + hash_str + ".dat"


def read_key(window, key):
    return glfw.get_key(window, key) == glfw.PRESS


def read_keys(window, turbo):
    result = {}
    result[nes.controller.BUTTON_A] = read_key(window, glfw.KEY_Z) | (turbo & read_key(window, glfw.KEY_A))
    result[nes.controller.BUTTON_B] = read_key(window, glfw.KEY_Z) | (turbo & read_key(window, glfw.KEY_A))
    result[nes.controller.BUTTON_SELECT] = read_key(window, glfw.KEY_RIGHT_SHIFT)
    result[nes.controller.BUTTON_START] = read_key(window, glfw.KEY_ENTER)
    result[nes.controller.BUTTON_UP] = read_key(window, glfw.KEY_UP)
    result[nes.controller.BUTTON_DOWN] = read_key(window, glfw.KEY_DOWN)
    result[nes.controller.BUTTON_LEFT] = read_key(window, glfw.KEY_LEFT)
    result[nes.controller.BUTTON_RIGHT] = read_key(window, glfw.KEY_RIGHT)
    return result


def read_joystick(joy, turbo):
    result = [False for _ in range(8)]
    if not glfw.joystick_present(joy):
        return result
    axes = glfw.get_joystick_axes(joy)
    buttons = glfw.get_joystick_buttons(joy)
    result[nes.controller.BUTTON_A] = (buttons[0] == 1) | (turbo & buttons[2] == 1)
    result[nes.controller.BUTTON_B] = (buttons[1] == 1) | (turbo & buttons[3] == 1)
    result[nes.controller.BUTTON_SELECT] = buttons[6] == 1
    result[nes.controller.BUTTON_START] = buttons[7] == 1
    result[nes.controller.BUTTON_UP] = axes[1] < -0.5
    result[nes.controller.BUTTON_DOWN] = axes[1] > 0.5
    result[nes.controller.BUTTON_LEFT] = axes[0] < -0.5
    result[nes.controller.BUTTON_RIGHT] = axes[0] > 0.5
    return result


def combine_buttons(a, b):
    print("cmb", a, b)
    result = [None for _ in range(8)]
    for i in range(8):
        result[i] = a[i] | b[i]
    return result


def create_texture():
    texture = glGenTextures(1)
    # glGenTextures(1, texture)
    glBindTexture(GL_TEXTURE_2D, texture)
    glTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
    glTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
    glTexParameter(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
    glTexParameter(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
    glBindTexture(GL_TEXTURE_2D, 0)
    return texture


def set_texture(im):
    glTexImage2D(
        GL_TEXTURE_2D, 0, GL_RGBA,
        int(im.size[0]), int(im.size[1]), 0,
        GL_RGBA, GL_UNSIGNED_BYTE, im.tobytes()
    )


def copy_image(src):
    dst = None
    # dst = Image.new(src.bounds)
    # draw.Draw(dst, dst.rect, src, image.zp, draw.src)
    return dst


def write_sram(filename ,sram):
    pass


def read_sram(filename):
    pass
