# coding: utf-8
import glfw
from OpenGL.GL import *
import ui.util
import ui.texture
import nes.controller
import os.path


BORDER = 10
MARGIN = 10
INITIAL_DELAY = 0.3
REPEAT_DELAY = 0.1
TYPE_DELAY = 0.5


class MenuView:
    def __init__(self, director, paths):
        self.director = director
        self.paths = paths
        self.texture = ui.texture.Texture.new_texture()
        self.nx = 0
        self.ny = 0
        self.i = 0
        self.j = 0
        self.scroll = 0
        self.t = 0.0
        self.buttons = [None for _ in range(8)]
        self.times = []
        self.type_buffer = ""
        self.type_time = 0.0

    def check_buttons(self):
        window = self.director.window
        k1 = ui.util.read_keys(window, False)
        j1 = ui.util.read_joystick(glfw.JOYSTICK_1, False)
        j2 = ui.util.read_joystick(glfw.JOYSTICK_2, False)
        buttons = ui.util.combine_buttons(ui.util.combine_buttons(j1, j2), k1)
        now = glfw.get_time()

        for i in range(len(buttons)):
            if buttons[i] and not self.buttons[i]:
                self.times[i] = now + INITIAL_DELAY
                self.on_press(i)
            elif not buttons[i] and self.buttons[i]:
                self.on_release(i)
            elif buttons[i] and now >= self.times[i]:
                self.times[i] = now + REPEAT_DELAY
                self.on_press(i)

        self.buttons = buttons

    def on_press(self, index):
        if index == nes.controller.BUTTON_UP:
            self.j -= 1
        elif index == nes.controller.BUTTON_DOWN:
            self.j += 1
        elif index == nes.controller.BUTTON_LEFT:
            self.i -= 1
        elif index == nes.controller.BUTTON_RIGHT:
            self.i += 1

        self.t = glfw.get_time()

    def on_release(self, index):
        if index == nes.controller.BUTTON_START:
            self.on_select()

    def on_select(self):
        index = self.nx * (self.j + self.scroll) + self.i
        if index >= len(self.paths):
            return
        self.director.play_game(self.paths[index])

    def on_char(self, char):
        now = glfw.get_time()
        if now > self.type_time:
            self.type_buffer = ""

        self.type_time = now + TYPE_DELAY
        self.type_buffer = (self.type_buffer + char).lower()

        for index, p in self.paths:
            p = os.path.split(p.lower())
            if p >= self.type_buffer:
                self.highlight(index)
                return

    def highlight(self, index):
        self.scroll = index / self.nx - (self.ny - 1) / 2
        self.clamp_scroll(False)
        self.i = index % self.nx
        self.j = (index - self.i)/self.nx - self.scroll

    def enter(self):
        glClearColor(0.333, 0.333, 0.333, 1)
        self.director.set_title("Select Game")
        glfw.set_char_callback(self.director.window, self.on_char)

    def exit(self):
        glfw.set_char_callback(self.director.window, None)

    def update(self, t):
        self.check_buttons()
        # TODO
        #self.texture.purge()

        window = self.director.window
        w, h = glfw.get_framebuffer_size(window)

        sx = 256 + MARGIN * 2
        sy = 240 + MARGIN * 2
        nx = int((w - BORDER * 2) / sx)
        ny = int((w - BORDER * 2) / sy)
        ox = (w-nx*sx)/2 + MARGIN
        oy = (h-ny*sy)/2 + MARGIN
        if nx < 1:
            nx = 1
        if ny < 1:
            ny = 1

        self.nx = nx
        self.ny = ny
        self.clamp_selection()
        glPushMatrix()
        glOrtho(0, float(w), float(h), 0, -1, 1)

        self.texture.bind()
        for j in range(ny):
            for i in range(nx):
                x = float(ox + i*sx)
                y = float(oy + j*sy)
                index = nx*(j+self.scroll)
                if index >= len(self.paths):
                    continue
                path = self.paths[index]
                tx, ty, tw, th = self.texture.look_up(path)
                MenuView.draw_thumbnail(x, y, tx, ty, tw, th)

        self.texture.unbind()
        if int((t-self.t)*4)%2 == 0:
            x = float(ox + self.i*sx)
            y = float(oy + self.j*sy)
            MenuView.draw_selection(x, y, 8, 4)
        glPopMatrix()

    def clamp_selection(self):
        if self.i < 0:
            self.i = self.nx - 1
        if self.i >= self.nx:
            self.i = 0
        if self.j < 0:
            self.j = 0
            self.scroll -= 1
        if self.j >= self.ny:
            self.j = self.ny - 1
            self.scroll += 1
        self.clamp_scroll(True)

    def clamp_scroll(self, wrap):
        n = len(self.paths)
        rows = n / self.nx
        if n * self.nx > 0:
            rows += 1
        max_scroll = rows - self.ny
        if self.scroll < 0:
            if wrap:
                self.scroll = max_scroll
                self.j = self.ny - 1
            else:
                self.scroll = 0
                self.j = 0
        if self.scroll > max_scroll:
            if wrap:
                self.scroll = 0
                self.j = 0
            else:
                self.scroll = max_scroll
                self.j = self.ny - 1

    @classmethod
    def draw_thumbnail(cls, x, y, tx, ty, tw, th):
        sx = x + 4
        sy = y + 4
        glDisable(GL_TEXTURE_2D)
        glColor3f(0.2, 0.2, 0.2)
        glBegin(GL_QUADS)
        glVertex2f(sx, sy)
        glVertex2f(sx+256, sy)
        glVertex2f(sx+256, sy+240)
        glVertex2f(sx, sy+240)
        glEnd()
        glEnable(GL_TEXTURE_2D)
        glColor3f(1, 1, 1)
        glBegin(GL_QUADS)
        glTexCoord2f(tx, ty)
        glVertex2f(x, y)
        glTexCoord2f(tx+tw, ty)
        glVertex2f(x+256, y)
        glTexCoord2f(tx+tw, ty+th)
        glVertex2f(x+256, y+240)
        glTexCoord2f(tx, ty+th)
        glVertex2f(x, y+240)
        glEnd()

    @classmethod
    def draw_selection(cls, x, y, p, w):
        glLineWidth(w)
        glBegin(GL_LINE_STRIP)
        glVertex2f(x-p, y-p)
        glVertex2f(x+256+p, y-p)
        glVertex2f(x+256+p, y+240+p)
        glVertex2f(x-p, y+240+p)
        glVertex2f(x-p, y-p)
        glEnd()

