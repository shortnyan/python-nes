# coding: utf-8

import glfw
from OpenGL.GL import *
import ui.texture
import ui.util

PADDING = 0


class GameView:
    def __init__(self, director, console, title, hash_string):
        self.director = director
        self.console = console
        self.title = title
        self.hash = hash_string
        self.texture = ui.util.create_texture()
        self.record = False
        self.frames = None

    def enter(self):
        glClearColor(0, 0, 0, 1)
        self.director.set_title(self.title)
        glfw.set_key_callback(self.director.window, self.on_key)

        # load state
        if self.console.load_state(ui.util.save_path(self.hash)):
            return
        else:
            self.console.reset()

        # load sram
        cartridge = self.console.cartridge
        if cartridge.battery != 0:
            path = ui.util.sram_path(self.hash)
            sram = ui.util.read_sram(ui.util.sram_path(self.hash))
            if sram is not None:
                cartridge.sram = sram

    def exit(self):
        glfw.set_key_callback(self.director.window, None)

        # save sram
        cartridge = self.console.cartrige
        if cartridge.battery != 0:
            ui.util.write_sram(ui.util.sram_path(self.hash), cartridge.sram)

        # save state
        self.console.save_state(ui.util.save_path(self.hash))

    def update(self, dt):
        if dt > 1:
            dt = 0
        window = self.director.window
        console = self.console

        # TODO
        """
        if joystick_reset(glfw.JOYSTICK_1):
            self.director.show_menu()
        if joystick_reset(glfw.JOYSTICK_2):
            self.director.show_menu
        if read_key(window, glfw.KEY_ESCAPE):
            self.director.show_menu
        """

        update_controllers(window, console)
        console.step_seconds(dt)
        glBindTexture(GL_TEXTURE_2D, self.texture)
        ui.util.set_texture(console.buffer())
        draw_buffer(self.director.window)
        glBindTexture(GL_TEXTURE_2D, 0)
        if self.record:
            self.frames.append(ui.util.copy_image(console.buffer()))

    def on_key(self, window, key, scancode, action, mods):
        if action == glfw.press:
            if key == glfw.KEY_SPACE:
                # TODO
                # creenshot(self.console.buffer())
                pass
            elif key == glfw.KEY_R:
                self.console.reset()
            elif key == glfw.KEY_TAB:
                if self.record:
                    self.record = False
                    animation(self.frames)
                    self.frames = None
                else:
                    self.record = True


def draw_buffer(window):
    w, h = glfw.get_framebuffer_size(window)
    s1 = float(w) / 256
    s2 = float(h) / 240
    f = float(1 - PADDING)
    if s1 >= s2:
        x = f * s2 / s1
        y = f
    else:
        x = f
        y = f * s1 / s2
    glBegin(GL_QUADS)
    glTexCoord2f(0, 1)
    glVertex2f(-x, -y)
    glTexCoord2f(1, 1)
    glVertex2f(x, -y)
    glTexCoord2f(1, 0)
    glVertex2f(x, y)
    glTexCoord2f(0, 0)
    glVertex2f(-x, y)
    glEnd()


def update_controllers(window, console):
    turbo = console.ppu.frame % 6 < 3
    k1 = ui.util.read_keys(window, turbo)
    j1 = ui.util.read_joystick(glfw.JOYSTICK_1, turbo)
    j2 = ui.util.read_joystick(glfw.JOYSTICK_2, turbo)
    print("k1", k1)
    print("j1", j1)
    print("j2", j2)
    console.set_buttons1(ui.util.combine_buttons(k1, j1))
    console.set_buttons2(j2)
