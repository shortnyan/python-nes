# coding: utf-8
import glfw
from OpenGL.GL import *
import ui.director

WIDTH = 256
HEIGHT = 240
SCALE = 3
TITLE = "NES"


def run(paths):
    # initialize glfw
    if not glfw.init():
        return

    # create window
    glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 2)
    glfw.window_hint(glfw.CONTEXT_VERSION_MINOR, 1)
    window = glfw.create_window(WIDTH*SCALE, HEIGHT*SCALE, TITLE, None, None)
    if not window:
        glfw.terminate()
        return
    glfw.make_context_current(window)

    # initialize gl
    """
    while not glfw.window_should_close(window):
        # Render here, e.g. using pyOpenGL

        # Swap front and back buffers
        glfw.swap_buffers(window)

        # Poll for and process events
        glfw.poll_events()
    """
    glEnable(GL_TEXTURE_2D)

    # run director
    director = ui.director.Director(window)
    director.start(paths)

