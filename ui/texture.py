# coding: utf-8
import ui.util
from OpenGL.GL import *

TEXTURE_SIZE = 4096
TEXTURE_DIM = TEXTURE_SIZE / 256
TEXTURE_COUNT = int(TEXTURE_DIM * TEXTURE_DIM)


class Texture:
    def __init__(self):
        self.texture = 0
        self.lookup = {}
        self.reverse = ["" for _ in range(TEXTURE_COUNT)]
        self.access = [0 for _ in range(TEXTURE_COUNT)]
        self.counter = 0
        self.ch = ""

    @classmethod
    def new_texture(cls):
        texture = ui.util.create_texture()
        glBindTexture(GL_TEXTURE_2D, texture)
        glTexImage2D(
            GL_TEXTURE_2D, 0, GL_RGBA,
            TEXTURE_SIZE, TEXTURE_SIZE,
            0, GL_RGBA, GL_UNSIGNED_BYTE, None
        )
        glBindTexture(GL_TEXTURE_2D, 0)

        t = Texture()
        t.texture = texture
        t.lookup = {}
        t.ch = ""
        return t

    #def purge:

    def bind(self):
        glBindTexture(GL_TEXTURE_2D, self.texture)

    def unbind(self):
        glBindTexture(GL_TEXTURE_2D, 0)

    def look_up(self, path):
        if path in self.lookup:
            index = self.lookup[path]
            return self.coord(index)
        else:
            return self.coord(self.load(path))

    def mark(self, index):
        self.counter += 1
        self.access[index] = self.counter

    def lru(self):
        min_index = 0
        min_value = self.counter + 1
        for i, n in enumerate(self.access):
            if n < min_value:
                min_index = i
                min_value = n
        return min_index

    def coord(self, index):
        x = float(index % TEXTURE_DIM) / TEXTURE_DIM
        y = float(index / TEXTURE_DIM) / TEXTURE_DIM
        dx = 1.0 / TEXTURE_DIM
        dy = dx * 240 / 256
        return [x, y, dx, dy]

    def load(self, path):
        index = self.lru()
        if index in self.reverse and self.reverse[index] in self.lookup:
            del self.lookup[self.reverse[index]]
        self.mark(index)
        self.lookup[path] = index
        self.reverse[index] = path
        x = int((index % TEXTURE_DIM) * 256)
        y = int((index % TEXTURE_DIM) * 256)
        im = ui.util.copy_image(self.load_thumbnail(path))
        # TODO
        # size = im.Rect.size()
        # glTexSubImage2D(GL_TEXTURE_2D, 0, x, y, size.X, size.Y, GL_RGBA, GL_UNSIGNED_BYTE, GL_PTR(im.pix))
        return index

    def load_thumbnail(self, rom_path):
        return ""
        # TODO
        """
        name = os.path.split(rom_path)
        name = trim
        name = replace
        im = create_generic_thumbnail(name)
        hash = hashfile(rom_path)

        file_name = thumbnail_path(hash)
        if
        """


