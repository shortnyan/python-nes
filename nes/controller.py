# coding: utf-8

BUTTON_A = 0
BUTTON_B = 1
BUTTON_SELECT = 2
BUTTON_START = 3
BUTTON_UP = 4
BUTTON_DOWN = 5
BUTTON_LEFT = 6
BUTTON_RIGHT = 7


class Controller:
    def __init__(self):
        self.buttons = []
        self.index = 0
        self.strobe = 0

    def set_buttons(self, buttons):
        self.buttons = buttons

    def read(self):
        value = 0
        print("ctr read", value, self.index, self.buttons)
        if self.index < 8:
            if self.buttons[self.index]:
                value = 1
        self.index += 1
        if (self.strobe & 1) == 1:
            self.index = 0
        print("ctr read value", value)
        return value

    def write(self, value):
        self.strobe = value
        if (self.strobe & 1) == 1:
            self.index = 0
        print("ctr write", value, self.strobe, self.index)
