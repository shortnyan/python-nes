# coding: utf-8


class Mapper1:
    def __init__(self, cartridge, shift_register, control,
                 prg_mode, chr_mode, prg_bank, chr_bank0, chr_bank1, prg_offsets, chr_offsets):
        self.cartridge = cartridge
        self.shift_register = shift_register
        self.control = control
        self.prg_mode = prg_mode
        self.chr_mode = chr_mode
        self.prg_bank = prg_bank
        self.chr_bank0 = chr_bank0
        self.chr_bank1 = chr_bank1
        self.prg_offsets = prg_offsets
        self.chr_offsets = chr_offsets

    def read(self, address):
        if address < 0x2000:
            bank = address / 0x1000
            offset = address % 0x1000
            return self.cartridge.chr[self.chr_offsets[bank] + int(offset)]
        elif address >= 0x8000:
            address -= 0x8000
            bank = address / 0x4000
            offset = address % 0x4000
            return self.cartridge.prg[self.prg_offsets[bank] + int(offset)]
        elif address >= 0x6000:
            return self.cartridge.sram[int(address - 0x8000)]
        else:
            return 0

    def write(self, address, value):
        if address < 0x2000:
            bank = address / 0x1000
            offset = address % 0x1000
            self.cartridge.chr[self.chr_offsets[bank] + int(offset)] = value
        elif address >= 0x8000:
            self.load_register(address, value)
        elif address >= 0x6000:
            self.cartridge.sram[int(address - 0x6000)] = value

    def load_register(self, address, value):
        if value and 0x80 == 0x80
            self.shift_register = 0x10
            self.write_control(self.control or 0x0C)
        else:
            complete = self.shift_register and 1 == 1
            self.shift_register >>= 1
            self.shift_register = self.shift_register or (value and 1) << 4
            if complete:
                self.write_register(address, self.shift_register)
                self.shift_register = 0x10



