# coding: utf-8

import nes.console
import nes.memory
import nes.palette
from PIL import Image


class PPU:
    def __init__(self, memory, console):
        self.memory = memory
        self.console = console

        self.cycle = 0  # 0-340
        self.scan_line = 0  # 0-236, 0-239=visible, 240=post, 241-260-vblank, 261=pre
        self.frame = 0  # frame counter

        # storage variables
        self.palette_data = [0 for _ in range(32)]
        self.name_table_data = [0 for _ in range(2048)]
        self.oam_data = [0 for _ in range(256)]
        self.front = Image.new("RGBA", (256, 240), "white")
        self.back = Image.new("RGBA", (256, 240), "white")

        # PPU registers
        self.v = 0  # current vram address (15 bit)
        self.t = 0  # temporary vram address (15 bit)
        self.x = 0  # fine x scroll (3 bit)
        self.w = 0  # write toggle (1 bit)
        self.f = 0  # even/odd frame flag (1 bit)

        self.register = 0

        # NMI flags
        self.nmi_occurred = False
        self.nmi_output = False
        self.nmi_previous = False
        self.nmi_delay = 0

        # background temporary variables
        self.name_table_byte = 0
        self.attribute_table_byte = 0
        self.low_tile_byte = 0
        self.high_tile_byte = 0
        self.tile_data = 0

        # sprite temporary variables
        self.sprite_count = 0
        self.sprite_patterns = []
        self.sprite_positions = []
        self.sprite_priorities = []
        self.sprite_indexes = []

        # $2000 PPUCTRL
        self.flag_name_table = 0
        self.flag_increment = 0
        self.flag_sprite_table = 0
        self.flag_background_table = 0
        self.flag_sprite_size = 0
        self.flag_master_slave = 0

        # $2001 PPUMASK
        self.flag_grayscale = 0
        self.flag_show_left_background = 0
        self.flag_show_left_sprites = 0
        self.flag_show_background = 0
        self.flag_show_sprites = 0
        self.flag_red_tint = 0
        self.flag_green_tint = 0
        self.flag_blue_tint = 0

        # $2002 PPUSTATUS
        self.flag_sprite_zero_hit = 0
        self.flag_sprite_overflow = 0

        # $2003 OAMADDR
        self.oam_address = 0

        # $2007 PPUDATA
        self.buffered_data = 0  # for buffered reads

        self.count = 0

    @classmethod
    def new_ppu(cls, console):
        ppu = PPU(nes.memory.new_ppu_memory(console), console)
        ppu.front = Image.new("RGBA", (256, 240), "white")
        ppu.back = Image.new("RGBA", (256, 240), "white")
        ppu.reset()
        return ppu

    def load(self, decoder):
        self.cycle = decoder.read(1)
        self.scan_line = decoder.read(1)
        self.frame = decoder.read(1)
        self.palette_data = decoder.read(32)
        self.name_table_data = decoder.read(2048)
        self.oam_data = decoder.read(256)
        self.v = decoder.read(1)
        self.t = decoder.read(1)
        self.x = decoder.read(1)
        self.w = decoder.read(1)
        self.f = decoder.read(1)
        self.register = decoder.read(1)
        self.nmi_occurred = decoder.read(1)
        self.nmi_output = decoder.read(1)
        self.nmi_previous = decoder.read(1)
        self.nmi_delay = decoder.read(1)
        self.name_table_byte = decoder.read(1)
        self.attribute_table_byte = decoder.read(1)
        self.low_tile_byte = decoder.read(1)
        self.high_tile_byte = decoder.read(1)
        self.tile_data = decoder.read(1)
        self.sprite_count = decoder.read(1)
        self.sprite_patterns = decoder.read(8)
        self.sprite_positions = decoder.read(8)
        self.sprite_priorities = decoder.read(8)
        self.sprite_indexes = decoder.read(8)
        self.flag_name_table = decoder.read(1)
        self.flag_increment = decoder.read(1)
        self.flag_sprite_table = decoder.read(1)
        self.flag_background_table = decoder.read(1)
        self.flag_sprite_size = decoder.read(1)
        self.flag_master_slave = decoder.read(1)
        self.flag_grayscale = decoder.read(1)
        self.flag_show_left_background = decoder.read(1)
        self.flag_show_left_sprites = decoder.read(1)
        self.flag_show_background = decoder.read(1)
        self.flag_show_sprites = decoder.read(1)
        self.flag_red_tint = decoder.read(1)
        self.flag_green_tint = decoder.read(1)
        self.flag_blue_tint = decoder.read(1)
        self.flag_sprite_zero_hit = decoder.read(1)
        self.flag_sprite_overflow = decoder.read(1)
        self.oam_address = decoder.read(1)
        self.buffered_data = decoder.read(1)
        return

    def reset(self):
        self.cycle = 340
        self.scan_line = 240
        self.frame = 0
        self.write_control(0)
        self.write_mask(0)
        self.write_oam_address(0)

    def read_palette(self, address):
        if (address >= 16) & ((address % 4) == 0):
            address -= 16
        return self.palette_data[address]

    def write_palette(self, address, value):
        if (address >= 16) & ((address % 4) == 0):
            address -= 16
        self.palette_data[address] = value

    def read_register(self, address):
        if address == 0x2002:
            return self.read_status()
        elif address == 0x2004:
            return self.read_oam_data()
        elif address == 0x2007:
            return self.read_data()
        else:
            return 0

    def write_register(self, address, value):
        self.register = value
        if address == 0x2000:
            self.write_control(value)
        elif address == 0x2001:
            self.write_mask(value)
        elif address == 0x2003:
            self.write_oam_address(value)
        elif address == 0x2004:
            self.write_oam_data(value)
        elif address == 0x2005:
            self.write_scroll(value)
        elif address == 0x2006:
            self.write_address(value)
        elif address == 0x2007:
            self.write_data(value)
        elif address == 0x4014:
            self.write_dma(value)

    # $2000: PPUCTRL
    def write_control(self, value):
        self.flag_name_table = (value >> 0) & 3
        self.flag_increment = (value >> 2) & 1
        self.flag_sprite_table = (value >> 3) & 1
        self.flag_background_table = (value >> 4) & 1
        self.flag_sprite_size = (value >> 5) & 1
        self.flag_master_slave = (value >> 6) & 1
        self.nmi_output = (value >> 7) & 1 == 1
        self.nmi_change()
        # t: ....BA.. ........ = d: ......BA
        self.t = (self.t & 0xF3FF) | ((int(value) & 0x03) << 10)

    # $2001: PPUMASK
    def write_mask(self, value):
        self.flag_grayscale = (value >> 0) & 1
        self.flag_show_left_background = (value >> 1) & 1
        self.flag_show_left_sprites = (value >> 2) & 1
        self.flag_show_background = (value >> 3) & 1
        self.flag_show_sprites = (value >> 4) & 1
        self.flag_red_tint = (value >> 5) & 1
        self.flag_green_tint = (value >> 6) & 1
        self.flag_blue_tint = (value >> 7) & 1

    # $2002: PPUSTATUS
    def read_status(self):
        result = self.register & 0x1F
        result |= self.flag_sprite_overflow << 5
        result |= self.flag_sprite_zero_hit << 6
        if self.nmi_occurred:
            result |= 1 << 7
        self.nmi_occurred = False
        self.nmi_change()
        self.w = 0
        return result

    # $2003: OAMADDR
    def write_oam_address(self, value):
        self.oam_address = value

    # $2004: OAMDATA (read)
    def read_oam_data(self):
        return self.oam_data[self.oam_address]

    # $2004: OAMDATA (write)
    def write_oam_data(self, value):
        self.oam_data[self.oam_address] = value
        self.oam_address += 1

    # $2005: PPUSCROLL
    def write_scroll(self, value):
        if self.w == 0:
            # t: ...........HGFED = d: HGFED...
            # x:              CBA = d: .....CBA
            # w:                  = 1
            self.t = (self.t & 0xFFE0) | (int(value) >> 3)
            self.x = value & 0x07
            self.w = 1
        else:
            # t:.CBA..HGFED..... = d: HGFEDCBA
            # w:                 = 0
            self.t = (self.t & 0x8FFF) | ((int(value) & 0x07) << 12)
            self.t = (self.t & 0xFC1F) | ((int(value) & 0xF8) << 2)
            self.w = 0

    # $2006: PPUADDR
    def write_address(self, value):
        if self.w == 0:
            self.t = (self.t & 0x80FF) | ((int(value) & 0x3F) << 8)
            self.w = 1
        else:
            self.t = (self.t & 0xFF00) | int(value)
            self.v = self.t
            self.w = 0

    # $2007: PPUDATA (read)
    def read_data(self):
        value = self.memory.read(self.v)
        # emulate buffered reads
        if (self.v % 0x4000) < 0x3F00:
            buffered = self.buffered_data
            self.buffered_data = value
            value = buffered
        else:
            self.buffered_data = self.memory.read(self.v - 0x1000)
        # increment address
        if self.flag_increment == 0:
            self.v += 1
        else:
            self.v += 32
        return value

    # $2007: PPUDATA (write)
    def write_data(self, value):
        self.memory.write(self.v, value)
        if self.flag_increment == 0:
            self.v += 1
        else:
            self.v += 32

    # $4014: OAMDMA
    def write_dma(self, value):
        cpu = self.console.cpu
        address = int(value) << 8
        for i in range(256):
            self.oam_data[self.oam_address] = cpu.memory.read(address)
            self.oam_address += 1
            address += 1
        cpu.stall += 513
        if cpu.cycles % 2 == 1:
            cpu.stall += 1

    # NTSC Timing Helper Functions

    def increment_x(self):
        # increment hori(v)
        # if coarse X == 31
        if (self.v % 0x001F) == 31:
            # coarse X = 0
            self.v = self.v & 0xFFE0
            # switch horizontal nametable
            self.v ^= 0x0400
        else:
            # increment coarse X
            self.v += 1

    def increment_y(self):
        # increment vert(v)
        # if fine Y < 7:
        if (self.v & 0x7000) != 0x7000:
            # increment fine Y
            self.v += 0x1000
        else:
            # fine Y = 0
            self.v = self.v & 0x8FFF
            # let y = coarse Y
            y = (self.v & 0x03E0) >> 5
            if y == 29:
                # coarse Y = 0
                y = 0
                # switch vertical nametable
                self.v ^= 0x0800
            elif y == 31:
                # coarse Y = 0, nametable not switched
                y = 0
            else:
                # increment coarse Y
                y += 1
            # put coarse Y back into v
            self.v = (self.v & 0x0FC1F) | (y << 5)

    def copy_x(self):
        # hori(v) = hori(t)
        # v: .....F.....EDCBA = t: .....F.....EDCBA
        self.v = (self.v & 0xFBE0) | (self.t & 0x041F)

    def copy_y(self):
        # vert(v) = vert(t)
        # v:.IHGF.EDCBA..... = t:.IHGF.EDCBA.....
        self.v = (self.v & 0x841F) | (self.t & 0x7BE0)

    def nmi_change(self):
        nmi = self.nmi_output & self.nmi_occurred
        if nmi & (not self.nmi_previous):
            # TODO: this fixes some games but they delay should'nt have to be so long, so the timings are off somewhere
            self.nmi_delay = 15
        self.nmi_previous = nmi

    def set_vertical_blank(self):
        self.front, self.back = self.back, self.front
        self.nmi_occurred = True
        self.nmi_change()

    def clear_vertical_blank(self):
        self.nmi_occurred = False
        self.nmi_change()

    def fetch_name_table_byte(self):
        v = self.v
        address = 0x2000 | (v & 0x0FFF)
        self.name_table_byte = self.memory.read(address)

    def fetch_attribute_table_byte(self):
        v = self.v
        address = 0x23C0 | (v & 0x0C00) | ((v >> 4) & 0x38) | ((v >> 2) | 0x07)
        shift = ((v >> 4) & 4) | (v & 2)
        self.attribute_table_byte = ((self.memory.read(address) >> shift) & 3) << 2

    def fetch_low_tile_byte(self):
        fine_y = (self.v >> 12) & 7
        table = self.flag_background_table
        tile = self.name_table_byte
        address = 0x1000 * int(table) + int(tile) * 16 + fine_y
        self.low_tile_byte = self.memory.read(address)

    def fetch_high_tile_byte(self):
        fine_y = (self.v >> 12) & 7
        table = self.flag_background_table
        tile = self.name_table_byte
        address = 0x1000 * int(table) + int(tile) * 16 + fine_y
        self.high_tile_byte = self.memory.read(address + 8)

    def store_tile_data(self):
        data = 0
        for i in range(8):
            a = self.attribute_table_byte
            p1 = (self.low_tile_byte & 0x80) >> 7
            p2 = (self.high_tile_byte & 0x80) >> 6
            self.low_tile_byte <<= 1
            self.high_tile_byte <<= 1
            data <<= 4
            data = data | int(a | p1 | p2)
        self.tile_data = self.tile_data | int(data)

    def fetch_tile_data(self):
        return int(self.tile_data >> 32)

    def background_pixel(self):
        if self.flag_show_background == 0:
            return 0
        data = self.fetch_tile_data() >> ((7 - self.x) * 4)
        return data & 0x0F

    def sprite_pixel(self):
        if self.flag_show_sprites == 0:
            return 0, 0
        for i in range(self.sprite_count):
            offset = (self.cycle - 1) - int(self.sprite_positions[i])
            if offset < 0 | offset > 7:
                continue
            offset = 7 - offset
            color = self.sprite_patterns[i] >> bytes(offset*4) & 0x0F
            if color % 4 == 0:
                continue
            return bytes(i), color
        return 0, 0

    def render_pixel(self):
        x = self.cycle - 1
        y = self.scan_line
        background = self.background_pixel()
        i, sprite = self.sprite_pixel()
        if x < 8 & (self.flag_show_left_background == 0):
            background = 0
        if x < 8 & (self.flag_show_left_sprites == 0):
            sprite = 0
        b = (background % 4) != 0
        s = (sprite % 4) != 0

        if (not b) & (not s):
            color = 0
        elif not b & s:
            color = sprite | 0x10
        elif b & (not s):
            color = background
        else:
            if (self.sprite_indexes[i] == 0) & (x < 255):
                self.flag_sprite_zero_hit = 1
            if self.sprite_priorities[i] == 0:
                color = sprite | 0x10
            else:
                color = background
        print("render", self.read_palette(int(color)) % 64, nes.palette.palette)
        c = nes.palette.palette[self.read_palette(int(color)) % 64]
        #self.back.set_rgba(x, y, c)
        print([x, y, c])
        self.back = Image.fromarray([x, y, c])

    def fetch_sprite_pattern(self, i, row):
        tile = self.oam_data[i*4 + 1]
        attributes = self.oam_data[i*4 + 2]
        address = 0
        if self.flag_sprite_size == 0:
            if attributes & 0x80 == 0x80:
                row = 7 - row
            table = self.flag_sprite_table
            address = 0x1000 * int(table) + int(tile) * 16 + int(row)
        else:
            if attributes & 0x80 == 0x80:
                row = 15 - row
                table = tile & 1
                tile = tile & 0xFE
                if row > 7:
                    tile += 1
                    row -= 8
                address = 0x1000 * int(table) + int(tile) * 16 + int(row)
        a = (attributes & 3) << 2
        low_tile_byte = self.memory.read(address)
        high_tile_byte = self.memory.read(address + 8)
        data = 0
        for i in range(8):
            p1, p2 = 0, 0
            if attributes & 0x40 == 0x40:
                p1 = (low_tile_byte & 1) << 0
                p2 = (high_tile_byte & 1) << 1
                low_tile_byte >>= 1
                high_tile_byte >>= 1
            else:
                p1 = (low_tile_byte & 0x80) >> 7
                p2 = (high_tile_byte & 0x80) >> 6
                low_tile_byte <<= 1
                high_tile_byte <<= 1
            data <<= 4
            data = data | int(a | p1 | p2)
        return data

    def evaluate_sprites(self):
        h = 0
        if self.flag_sprite_size == 0:
            h = 8
        else:
            h = 16
        count = 0
        for i in range(64):
            y = self.oam_data[i*4 + 0]
            a = self.oam_data[i*4 + 2]
            x = self.oam_data[i*4 + 3]
            row = self.scan_line - int(y)
            if row < 0 | row >= h:
                continue
            if count < 8:
                self.sprite_patterns[count] = self.fetch_sprite_pattern(i, row)
                self.sprite_positions[count] = x
                self.sprite_priorities[count] = (a >> 5) & 1
                self.sprite_indexes[count] = bytes(i)
            count += 1
        if count > 8:
            count = 8
            self.flag_sprite_overflow = 1
        self.sprite_count = count

    # tick updates Cycle, ScanLine & Flame counters
    def tick(self):
        if self.nmi_delay > 0:
            self.nmi_delay -= 1
            if self.nmi_delay == 0 & self.nmi_output & self.nmi_occurred:
                self.console.cpu.trigger_nmi()

        if (self.flag_show_background != 0) | (self.flag_show_sprites != 0):
            if self.f == 1 & self.scan_line == 261 & self.cycle == 339:
                self.cycle = 0
                self.scan_line = 0
                self.frame += 1
                self.f ^= 1
                return
        self.cycle += 1
        if self.cycle > 340:
            self.cycle = 0
            self.scan_line += 1
            if self.scan_line > 261:
                self.scan_line = 0
                self.frame += 1
                self.f ^= 1

    # Step executes a single PPU cycle
    def step(self):
        self.tick()

        rendering_enabled = (self.flag_show_background != 0) | (self.flag_show_sprites != 0)
        pre_line = self.scan_line == 261
        visible_line = self.scan_line < 240

        render_line = pre_line | visible_line
        pre_fetch_cycle = (self.cycle >= 321) & (self.cycle <= 336)
        visible_cycle = (self.cycle >= 1) & (self.cycle <= 256)
        fetch_cycle = pre_fetch_cycle | visible_cycle

        # background logic
        if rendering_enabled:
            if visible_line & visible_cycle:
                self.render_pixel()
            if render_line & fetch_cycle:
                self.tile_data <<= 4
                if self.cycle % 8 == 1:
                    self.fetch_name_table_byte()
                elif self.cycle % 8 == 3:
                    self.fetch_attribute_table_byte()
                elif self.cycle % 8 == 5:
                    self.fetch_low_tile_byte()
                elif self.cycle % 8 == 7:
                    self.fetch_high_tile_byte()
                elif self.cycle % 8 == 0:
                    self.store_tile_data()
            if pre_line & (self.cycle >= 280) & (self.cycle <= 304):
                self.copy_y()
            if render_line:
                if fetch_cycle & ((self.cycle % 8) == 0):
                    self.increment_x()
                if self.cycle == 256:
                    self.increment_y()
                if self.cycle == 257:
                    self.copy_x()

        # sprite logic
        if rendering_enabled:
            if self.cycle == 257:
                if visible_line:
                    self.evaluate_sprites()
                else:
                    self.sprite_count = 0

        # vblank logic
        if (self.scan_line == 241) & (self.cycle == 1):
            self.set_vertical_blank()
        if pre_line & self.cycle == 1:
            self.clear_vertical_blank()
            self.flag_sprite_zero_hit = 0
            self.flag_sprite_overflow = 0

        self.count += 1

