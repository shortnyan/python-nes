# coding: utf-8
import nes.memory

CPU_FREQUENCY = 1789773

# interrupt types
INTERRUPT_NONE = 1
INTERRUPT_NMI = 2
INTERRUPT_IRQ = 3


# addressing modes
MODE_ABSOLUTE = 1
MODE_ABSOLUTE_X = 2
MODE_ABSOLUTE_Y = 3
MODE_ACCUMULATOR = 4
MODE_IMMEDIATE = 5
MODE_IMPLIED = 6
MODE_INDEXED_INDIRECT = 7
MODE_INDIRECT = 8
MODE_INDIRECT_INDEXED = 9
MODE_RELATIVE = 10
MODE_ZERO_PAGE = 11
MODE_ZERO_PAGE_X = 12
MODE_ZERO_PAGE_Y = 13

# instruction_modes indicates the addressing mode for each instruction
instruction_modes = [
    6, 7, 6, 7, 11, 11, 11, 11, 6, 5, 4, 5, 1, 1, 1, 1,
    10, 9, 6, 9, 12, 12, 12, 12, 6, 3, 6, 3, 2, 2, 2, 2,
    1, 7, 6, 7, 11, 11, 11, 11, 6, 5, 4, 5, 1, 1, 1, 1,
    10, 9, 6, 9, 12, 12, 12, 12, 6, 3, 6, 3, 2, 2, 2, 2,
    6, 7, 6, 7, 11, 11, 11, 11, 6, 5, 4, 5, 1, 1, 1, 1,
    10, 9, 6, 9, 12, 12, 12, 12, 6, 3, 6, 3, 2, 2, 2, 2,
    6, 7, 6, 7, 11, 11, 11, 11, 6, 5, 4, 5, 8, 1, 1, 1,
    10, 9, 6, 9, 12, 12, 12, 12, 6, 3, 6, 3, 2, 2, 2, 2,
    5, 7, 5, 7, 11, 11, 11, 11, 6, 5, 6, 5, 1, 1, 1, 1,
    10, 9, 6, 9, 12, 12, 13, 13, 6, 3, 6, 3, 2, 2, 3, 3,
    5, 7, 5, 7, 11, 11, 11, 11, 6, 5, 6, 5, 1, 1, 1, 1,
    10, 9, 6, 9, 12, 12, 13, 13, 6, 3, 6, 3, 2, 2, 3, 3,
    5, 7, 5, 7, 11, 11, 11, 11, 6, 5, 6, 5, 1, 1, 1, 1,
    10, 9, 6, 9, 12, 12, 12, 12, 6, 3, 6, 3, 2, 2, 2, 2,
    5, 7, 5, 7, 11, 11, 11, 11, 6, 5, 6, 5, 1, 1, 1, 1,
    10, 9, 6, 9, 12, 12, 12, 12, 6, 3, 6, 3, 2, 2, 2, 2,
]

# instruction_sizes indicates the size of each instruction in bytes
instruction_sizes = [
    1, 2, 0, 0, 2, 2, 2, 0, 1, 2, 1, 0, 3, 3, 3, 0,
    2, 2, 0, 0, 2, 2, 2, 0, 1, 3, 1, 0, 3, 3, 3, 0,
    3, 2, 0, 0, 2, 2, 2, 0, 1, 2, 1, 0, 3, 3, 3, 0,
    2, 2, 0, 0, 2, 2, 2, 0, 1, 3, 1, 0, 3, 3, 3, 0,
    1, 2, 0, 0, 2, 2, 2, 0, 1, 2, 1, 0, 3, 3, 3, 0,
    2, 2, 0, 0, 2, 2, 2, 0, 1, 3, 1, 0, 3, 3, 3, 0,
    1, 2, 0, 0, 2, 2, 2, 0, 1, 2, 1, 0, 3, 3, 3, 0,
    2, 2, 0, 0, 2, 2, 2, 0, 1, 3, 1, 0, 3, 3, 3, 0,
    2, 2, 0, 0, 2, 2, 2, 0, 1, 0, 1, 0, 3, 3, 3, 0,
    2, 2, 0, 0, 2, 2, 2, 0, 1, 3, 1, 0, 0, 3, 0, 0,
    2, 2, 2, 0, 2, 2, 2, 0, 1, 2, 1, 0, 3, 3, 3, 0,
    2, 2, 0, 0, 2, 2, 2, 0, 1, 3, 1, 0, 3, 3, 3, 0,
    2, 2, 0, 0, 2, 2, 2, 0, 1, 2, 1, 0, 3, 3, 3, 0,
    2, 2, 0, 0, 2, 2, 2, 0, 1, 3, 1, 0, 3, 3, 3, 0,
    2, 2, 0, 0, 2, 2, 2, 0, 1, 2, 1, 0, 3, 3, 3, 0,
    2, 2, 0, 0, 2, 2, 2, 0, 1, 3, 1, 0, 3, 3, 3, 0,
]

# instruction_cycles indicates the number of cycles used by each instruction,
# not including conditional cycles
instruction_cycles = [
    7, 6, 2, 8, 3, 3, 5, 5, 3, 2, 2, 2, 4, 4, 6, 6,
    2, 5, 2, 8, 4, 4, 6, 6, 2, 4, 2, 7, 4, 4, 7, 7,
    6, 6, 2, 8, 3, 3, 5, 5, 4, 2, 2, 2, 4, 4, 6, 6,
    2, 5, 2, 8, 4, 4, 6, 6, 2, 4, 2, 7, 4, 4, 7, 7,
    6, 6, 2, 8, 3, 3, 5, 5, 3, 2, 2, 2, 3, 4, 6, 6,
    2, 5, 2, 8, 4, 4, 6, 6, 2, 4, 2, 7, 4, 4, 7, 7,
    6, 6, 2, 8, 3, 3, 5, 5, 4, 2, 2, 2, 5, 4, 6, 6,
    2, 5, 2, 8, 4, 4, 6, 6, 2, 4, 2, 7, 4, 4, 7, 7,
    2, 6, 2, 6, 3, 3, 3, 3, 2, 2, 2, 2, 4, 4, 4, 4,
    2, 6, 2, 6, 4, 4, 4, 4, 2, 5, 2, 5, 5, 5, 5, 5,
    2, 6, 2, 6, 3, 3, 3, 3, 2, 2, 2, 2, 4, 4, 4, 4,
    2, 5, 2, 5, 4, 4, 4, 4, 2, 4, 2, 4, 4, 4, 4, 4,
    2, 6, 2, 8, 3, 3, 5, 5, 2, 2, 2, 2, 4, 4, 6, 6,
    2, 5, 2, 8, 4, 4, 6, 6, 2, 4, 2, 7, 4, 4, 7, 7,
    2, 6, 2, 8, 3, 3, 5, 5, 2, 2, 2, 2, 4, 4, 6, 6,
    2, 5, 2, 8, 4, 4, 6, 6, 2, 4, 2, 7, 4, 4, 7, 7,
]

# instruction_page_cycles indicates the number of cycles used by each instruction when a page is crossed
instruction_page_cycles = [
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0,
]

# instruction_names indicates the name of each instruction
instruction_names = [
    "BRK", "ORA", "KIL", "SLO", "NOP", "ORA", "ASL", "SLO",
    "PHP", "ORA", "ASL", "ANC", "NOP", "ORA", "ASL", "SLO",
    "BPL", "ORA", "KIL", "SLO", "NOP", "ORA", "ASL", "SLO",
    "CLC", "ORA", "NOP", "SLO", "NOP", "ORA", "ASL", "SLO",
    "JSR", "AND", "KIL", "RLA", "BIT", "AND", "ROL", "RLA",
    "PLP", "AND", "ROL", "ANC", "BIT", "AND", "ROL", "RLA",
    "BMI", "AND", "KIL", "RLA", "NOP", "AND", "ROL", "RLA",
    "SEC", "AND", "NOP", "RLA", "NOP", "AND", "ROL", "RLA",
    "RTI", "EOR", "KIL", "SRE", "NOP", "EOR", "LSR", "SRE",
    "PHA", "EOR", "LSR", "ALR", "JMP", "EOR", "LSR", "SRE",
    "BVC", "EOR", "KIL", "SRE", "NOP", "EOR", "LSR", "SRE",
    "CLI", "EOR", "NOP", "SRE", "NOP", "EOR", "LSR", "SRE",
    "RTS", "ADC", "KIL", "RRA", "NOP", "ADC", "ROR", "RRA",
    "PLA", "ADC", "ROR", "ARR", "JMP", "ADC", "ROR", "RRA",
    "BVS", "ADC", "KIL", "RRA", "NOP", "ADC", "ROR", "RRA",
    "SEI", "ADC", "NOP", "RRA", "NOP", "ADC", "ROR", "RRA",
    "NOP", "STA", "NOP", "SAX", "STY", "STA", "STX", "SAX",
    "DEY", "NOP", "TXA", "XAA", "STY", "STA", "STX", "SAX",
    "BCC", "STA", "KIL", "AHX", "STY", "STA", "STX", "SAX",
    "TYA", "STA", "TXS", "TAS", "SHY", "STA", "SHX", "AHX",
    "LDY", "LDA", "LDX", "LAX", "LDY", "LDA", "LDX", "LAX",
    "TAY", "LDA", "TAX", "LAX", "LDY", "LDA", "LDX", "LAX",
    "BCS", "LDA", "KIL", "LAX", "LDY", "LDA", "LDX", "LAX",
    "CLV", "LDA", "TSX", "LAS", "LDY", "LDA", "LDX", "LAX",
    "CPY", "CMP", "NOP", "DCP", "CPY", "CMP", "DEC", "DCP",
    "INY", "CMP", "DEX", "AXS", "CPY", "CMP", "DEC", "DCP",
    "BNE", "CMP", "KIL", "DCP", "NOP", "CMP", "DEC", "DCP",
    "CLD", "CMP", "NOP", "DCP", "NOP", "CMP", "DEC", "DCP",
    "CPX", "SBC", "NOP", "ISC", "CPX", "SBC", "INC", "ISC",
    "INX", "SBC", "NOP", "SBC", "CPX", "SBC", "INC", "ISC",
    "BEQ", "SBC", "KIL", "ISC", "NOP", "SBC", "INC", "ISC",
    "SED", "SBC", "NOP", "ISC", "NOP", "SBC", "INC", "ISC",
]


# StepInfo contains information that the instruction functions use
class StepInfo:
    def __init__(self, address, pc, mode):
        self.address = address
        self.pc = pc
        self.mode = mode


class CPU:
    def __init__(self, memory):
        self.memory = memory  # memory
        self.cycles = 0       # number of cycles
        self.pc = 0           # program counter
        self.sp = 0           # stack pointer
        self.a = 0            # accumulator
        self.x = 0            # x register
        self.y = 0            # y register
        self.c = 0            # carry flag
        self.z = 0            # zero flag
        self.i = 0            # interrupt disable flag
        self.d = 0            # decimal mode flag
        self.b = 0            # break command flag
        self.u = 0            # unused flag
        self.v = 0            # overflow flag
        self.n = 0            # negative flag
        self.interrupt = 0    # interrupt type to perform
        self.stall = 0        # number of cycles to stall
        self.table = None
        self.count = 0

    @classmethod
    def new_cpu(cls, console):
        cpu = CPU(nes.memory.new_cpu_memory(console))
        cpu.create_table()
        cpu.reset()
        return cpu

    # create_table builds a function table for each instruction
    def create_table(self):
        self.table = [
            self.brk, self.ora, self.kil, self.slo, self.nop, self.ora, self.asl, self.slo,
            self.php, self.ora, self.asl, self.anc, self.nop, self.ora, self.asl, self.slo,
            self.bpl, self.ora, self.kil, self.slo, self.nop, self.ora, self.asl, self.slo,
            self.clc, self.ora, self.nop, self.slo, self.nop, self.ora, self.asl, self.slo,
            self.jsr, self._and, self.kil, self.rla, self.bit, self._and, self.rol, self.rla,
            self.plp, self._and, self.rol, self.anc, self.bit, self._and, self.rol, self.rla,
            self.bmi, self._and, self.kil, self.rla, self.nop, self._and, self.rol, self.rla,
            self.sec, self._and, self.nop, self.rla, self.nop, self._and, self.rol, self.rla,
            self.rti, self.eor, self.kil, self.sre, self.nop, self.eor, self.lsr, self.sre,
            self.pha, self.eor, self.lsr, self.alr, self.jmp, self.eor, self.lsr, self.sre,
            self.bvc, self.eor, self.kil, self.sre, self.nop, self.eor, self.lsr, self.sre,
            self.cli, self.eor, self.nop, self.sre, self.nop, self.eor, self.lsr, self.sre,
            self.rts, self.adc, self.kil, self.rra, self.nop, self.adc, self.ror, self.rra,
            self.pla, self.adc, self.ror, self.arr, self.jmp, self.adc, self.ror, self.rra,
            self.bvs, self.adc, self.kil, self.rra, self.nop, self.adc, self.ror, self.rra,
            self.sei, self.adc, self.nop, self.rra, self.nop, self.adc, self.ror, self.rra,
            self.nop, self.sta, self.nop, self.sax, self.sty, self.sta, self.stx, self.sax,
            self.dey, self.nop, self.txa, self.xaa, self.sty, self.sta, self.stx, self.sax,
            self.bcc, self.sta, self.kil, self.ahx, self.sty, self.sta, self.stx, self.sax,
            self.tya, self.sta, self.txs, self.tas, self.shy, self.sta, self.shx, self.ahx,
            self.ldy, self.lda, self.ldx, self.lax, self.ldy, self.lda, self.ldx, self.lax,
            self.tay, self.lda, self.tax, self.lax, self.ldy, self.lda, self.ldx, self.lax,
            self.bcs, self.lda, self.kil, self.lax, self.ldy, self.lda, self.ldx, self.lax,
            self.clv, self.lda, self.tsx, self.las, self.ldy, self.lda, self.ldx, self.lax,
            self.cpy, self.cmp, self.nop, self.dcp, self.cpy, self.cmp, self.dec, self.dcp,
            self.iny, self.cmp, self.dex, self.axs, self.cpy, self.cmp, self.dec, self.dcp,
            self.bne, self.cmp, self.kil, self.dcp, self.nop, self.cmp, self.dec, self.dcp,
            self.cld, self.cmp, self.nop, self.dcp, self.nop, self.cmp, self.dec, self.dcp,
            self.cpx, self.sbc, self.nop, self.isc, self.cpx, self.sbc, self.inc, self.isc,
            self.inx, self.sbc, self.nop, self.sbc, self.cpx, self.sbc, self.inc, self.isc,
            self.beq, self.sbc, self.kil, self.isc, self.nop, self.sbc, self.inc, self.isc,
            self.sed, self.sbc, self.nop, self.isc, self.nop, self.sbc, self.inc, self.isc,
        ]

    def load(self, decoder):
        self.cycles = decoder.read(1)
        self.pc = decoder.read(1)

    # reset resets the CPU to its initial power-up state
    def reset(self):
        self.pc = self.read16(0xFFFC)
        self.sp = 0xFD
        self.set_flags(0x24)

    # print_instruction prints the current CPU state
    def print_instruction(self):
        opcode = self.memory.read(self.pc)
        bytes = instruction_sizes[opcode]
        name = instruction_names[opcode]

        w0 = "{0:02X}".format(self.memory.read(self.pc + 0))
        w1 = "{0:02X}".format(self.memory.read(self.pc + 1))
        w2 = "{0:02X}".format(self.memory.read(self.pc + 2))
        if bytes < 2:
            w1 = "  "
        if bytes < 3:
            w2 = "  "

        print("{0:4X}  {1} {2} {3} {4} {5:10s} A:{6:02X} X:{7:02X} Y:{8:02X} P:{9:02X} SP:{10:02X} CYC:{11} count:{12:4d} mode:{13}".format(
            self.pc, w0, w1, w2, name, "",
            self.a, self.x, self.y, self.flags(), self.sp, (self.cycles * 3) % 341, self.count,
            instruction_modes[opcode]
        ))

    # pages_differ returns true if the two addresses references different pages
    def pages_differ(self, a, b):
        return (a & 0xFF00) != (b & 0XFF00)

    # add_branch_cycles adds a cycle for taking a branch
    # and adds another cycle if the branch jumps to a new page
    def add_branch_cycles(self, info: StepInfo):
        self.cycles += 1
        if self.pages_differ(info.pc, info.address):
            self.cycles += 1

    def compare(self, a, b):
        if (a - b) < 0:
            c = (a - b) % 256
        else:
            c = a - b
        print("compare", a, b, c)
        self.set_zn(c)
        if a >= b:
            self.c = 1
        else:
            self.c = 0

    # Read16 read two bytes using Read to return a double-word value
    def read16(self, address):
        lo = int(self.memory.read(address))
        hi = int(self.memory.read(address + 1))
        temp = hi << 8 | lo
        return temp

    # read16bug emulates a 6502 bug that caused the low byte to wrap without incrementing the high bytes
    def read16bug(self, address):
        a = address
        b = (a & 0xFF00) | int(a+1)
        lo = self.memory.read(a)
        hi = self.memory.read(b)
        ret = (int(hi) << 8) | int(lo)
        print("read16bug", a, b, lo, hi, ret)
        return ret

    # push pushes a byte onto the stack
    def push(self, value):
        self.memory.write((0x100 | int(self.sp)), value)
        self.sp -= 1

    # pull pops a byte from the stack
    def pull(self):
        self.sp += 1
        print("pull", (0x100 | int(self.sp)))
        return self.memory.read(0x100 | int(self.sp))

    # push16 pushes two bytes onto the stack
    def push16(self, value):
        hi = value >> 8
        lo = value & 0xFF
        self.push(hi)
        self.push(lo)

    # pull16 pops two bytes from the stack
    def pull16(self):
        lo = self.pull()
        hi = self.pull()
        return hi << 8 | lo

    # flags returns the processor status flag
    def flags(self):
        flags = 0
        flags |= self.c << 0
        flags |= self.z << 1
        flags |= self.i << 2
        flags |= self.d << 3
        flags |= self.b << 4
        flags |= self.u << 5
        flags |= self.v << 6
        flags |= self.n << 7
        return flags

    # set_flags sets the processor status flags
    def set_flags(self, flags):
        self.c = (flags >> 0) & 1
        self.z = (flags >> 1) & 1
        self.i = (flags >> 2) & 1
        self.d = (flags >> 3) & 1
        self.b = (flags >> 4) & 1
        self.u = (flags >> 5) & 1
        self.v = (flags >> 6) & 1
        self.n = (flags >> 7) & 1

    # set_z sets the zero flag if the argument is zero
    def set_z(self, value):
        if value == 0:
            self.z = 1
        else:
            self.z = 0

    # set_n sets the negative flag is the argument is negative (high bit is set)
    def set_n(self, value):
        print("setn", value)
        if value < 0:
            exit(0)
        if (value & 0x80) != 0:
            self.n = 1
        else:
            self.n = 0

    # set_zn sets the zero flag and negative_flag
    def set_zn(self, value):
        self.set_z(value)
        self.set_n(value)

    # trigger_nmi causes a non-maskable interrupt to occur on the next cycle
    def trigger_nmi(self):
        self.interrupt = INTERRUPT_NMI

    # trigger_irq causes an IRQ interrupt to occur on the next cycle
    def trigger_irq(self):
        if self.i == 0:
            self.interrupt = INTERRUPT_IRQ

    # StepInfo contains information that the instruction functions use
    class StepInfo:
        def __init__(self, address, pc, mode):
            self.address = address
            self.pc = pc
            self.mode = mode

    # step executes a single CPU instruction
    def step(self):
        self.count += 1

        if self.stall > 0:
            self.stall -= 1
            return 1

        self.print_instruction()
        cycles = self.cycles

        if self.interrupt == INTERRUPT_NMI:
            self.nmi()
        elif self.interrupt == INTERRUPT_IRQ:
            self.irq()
        self.interrupt = INTERRUPT_NONE

        opcode = self.memory.read(self.pc)
        mode = instruction_modes[opcode]

        address = 0
        page_crossed = False
        if mode == MODE_ABSOLUTE:
            address = self.read16(self.pc + 1)
        elif mode == MODE_ABSOLUTE_X:
            address = self.read16(self.pc + 1) + int(self.x)
            page_crossed = self.pages_differ(address - int(self.x), address)
        elif mode == MODE_ABSOLUTE_Y:
            address = self.read16(self.pc + 1) + int(self.y)
            page_crossed = self.pages_differ(address - int(self.y), address)
        elif mode == MODE_ACCUMULATOR:
            address = 0
        elif mode == MODE_IMMEDIATE:
            address = self.pc + 1
        elif mode == MODE_IMPLIED:
            address = 0
        elif mode == MODE_INDEXED_INDIRECT:
            address = self.read16bug(int(self.memory.read(self.pc + 1) + self.x))
        elif mode == MODE_INDIRECT:
            address = self.read16bug(self.read16(self.pc + 1))
        elif mode == MODE_INDIRECT_INDEXED:
            address = self.read16bug(int(self.memory.read(self.pc + 1))) + int(self.y)
            page_crossed = self.pages_differ(address - int(self.y), address)
        elif mode == MODE_RELATIVE:
            offset = int(self.memory.read(self.pc + 1))
            if offset < 0x80:
                address = self.pc + 2 + offset
            else:
                address = self.pc + 2 + offset - 0x100
        elif mode == MODE_ZERO_PAGE:
            address = int(self.memory.read(self.pc + 1))
        elif mode == MODE_ZERO_PAGE_X:
            address = int(self.memory.read(self.pc + 1) + self.x)
        elif mode == MODE_ZERO_PAGE_Y:
            address = int(self.memory.read(self.pc + 1) + self.y)

        self.pc += instruction_sizes[opcode]
        self.cycles += instruction_cycles[opcode]
        if page_crossed:
            self.cycles += instruction_page_cycles[opcode]
        info = StepInfo(address, self.pc, mode)

        self.table[opcode](info)

        return self.cycles - cycles

    # NMI - Non-Maskable Interrupt
    def nmi(self):
        self.push16(self.pc)
        self.php(None)
        self.pc = self.read16(0XFFFA)
        self.i = 1
        self.cycles += 7

    # IRQ - IRQ Interrupt
    def irq(self):
        self.push16(self.pc)
        self.php(None)
        self.pc = self.read16(0xFFFE)
        self.i = 1
        self.cycles += 7

    # ADC - Add with carry
    def adc(self, info: StepInfo):
        a = self.a
        b = self.memory.read(info.address)
        c = self.c
        self.a = (int(a) + int(b) + int(c)) % 256
        self.set_z(self.a)
        if int(a) + int(b) + int(c) > 0xFF:
            self.c = 1
        else:
            self.c = 0
        if (((a ^ b) & 0x80) == 0) & (((a ^ self.a) & 0x80) != 0):
            self.v = 1
        else:
            self.v = 0

    # AND - Logical AND
    def _and(self, info: StepInfo):
        self.a = self.a & self.memory.read(info.address)
        self.set_zn(self.a)

    # ASL - Arithmetic Shift Left
    def asl(self, info: StepInfo):

        if info.mode == MODE_ACCUMULATOR:
            self.c = (self.a >> 7) & 1
            self.a = (self.a << 1) % 256
            self.set_zn(self.a)
        else:
            value = self.memory.read(info.address)
            self.c = (value >> 7) & 1
            value = (value << 1) % 256
            self.memory.write(info.address, value)
            self.set_zn(value)

    # BCC - Branch if Carry Clear
    def bcc(self, info: StepInfo):
        if self.c == 0:
            self.pc = info.address
            self.add_branch_cycles(info)

    # BCS - Branch if Carry Set
    def bcs(self, info: StepInfo):
        if not self.c == 0:
            self.pc = info.address
            self.add_branch_cycles(info)

    # BEQ - Branch if Equal
    def beq(self, info: StepInfo):
        if not self.z == 0:
            self.pc = info.address
            self.add_branch_cycles(info)

    # BIT - Bit Test
    def bit(self, info: StepInfo):
        value = self.memory.read(info.address)
        self.v = (value >> 6) & 1
        self.set_z(value & self.a)
        self.set_n(value)

    # BMI - Branch if Minus
    def bmi(self, info: StepInfo):
        if not self.n == 0:
            self.pc = info.address
            self.add_branch_cycles(info)

    # BNE - Branch if Not Equal
    def bne(self, info: StepInfo):
        if self.z == 0:
            self.pc = info.address
            self.add_branch_cycles(info)

    # BPL - Branch if Positive
    def bpl(self, info: StepInfo):
        if self.n == 0:
            self.pc = info.address
            self.add_branch_cycles(info)

    # BRK - Force Interrupt
    def brk(self, info: StepInfo):
        self.push16(self.pc)
        self.php(info)
        self.sei(info)
        self.pc = self.read16(0xFFFE)

    # BVC - Branch if Overflow Clear
    def bvc(self, info: StepInfo):
        if self.v == 0:
            self.pc = info.address
            self.add_branch_cycles(info)

    # BVS - Branch if Overflow set
    def bvs(self, info: StepInfo):
        if not self.v == 0:
            self.pc = info.address
            self.add_branch_cycles(info)

    # CLC - Clear Carry Flag
    def clc(self, info: StepInfo):
        self.c = 0

    # CLD - Clear Decimal Mode
    def cld(self, info: StepInfo):
        self.d = 0

    # CLI - Clear Interrupt Disable
    def cli(self, info: StepInfo):
        self.i = 0

    # CLV - Clear Overflow Flag
    def clv(self, info: StepInfo):
        self.v = 0

    # CMP - Compare
    def cmp(self, info: StepInfo):
        value = self.memory.read(info.address)
        self.compare(self.a, value)

    # CPX - Compare X Register
    def cpx(self, info: StepInfo):
        value = self.memory.read(info.address)
        print("cpx", self.x, value, info.address)
        self.compare(self.x, value)

    # CPY - Compare Y Register
    def cpy(self, info: StepInfo):
        value = self.memory.read(info.address)
        self.compare(self.y, value)

    # DEC - Decrement Memory
    def dec(self, info: StepInfo):
        value = self.memory.read(info.address) - 1
        print("dec", info.address, value)
        self.memory.write(info.address, value)
        self.set_zn(value)

    # DEX - Decrement X Register
    def dex(self, info: StepInfo):
        self.x -= 1
        if self.x < 0:
            self.x += 256
        self.set_zn(self.x)

    # DEY - Decrement Y Register
    def dey(self, info: StepInfo):
        self.y -= 1
        if self.y < 0:
            self.y += 256
        self.set_zn(self.y)

    # EOR - Exclusive OR
    def eor(self, info: StepInfo):
        self.a = self.a ^ self.memory.read(info.address)
        self.set_zn(self.a)

    # INC - Increment Memory
    def inc(self, info: StepInfo):
        value = self.memory.read(info.address) + 1
        print("inc", info.address, value)
        self.memory.write(info.address, value)
        self.set_zn(value)

    # INX - Increment X Register
    def inx(self, info: StepInfo):
        self.x += 1
        if self.x > 255:
            self.x = 0
        self.set_zn(self.x)

    # INY - Increment Y Register
    def iny(self, info: StepInfo):
        self.y += 1
        if self.y > 255:
            self.y = 0
        self.set_zn(self.y)

    # JMP - Jump
    def jmp(self, info: StepInfo):
        self.pc = info.address

    # JSR - Jump to Subroutine
    def jsr(self, info: StepInfo):
        self.push16(self.pc - 1)
        self.pc = info.address

    # LDA - Load Accumulator
    def lda(self, info: StepInfo):
        self.a = self.memory.read(info.address)
        print("ldaa", self.a, self.memory.read(info.address), info.address)
        self.set_zn(self.a)

    # LDX - Load X Register
    def ldx(self, info: StepInfo):
        self.x = self.memory.read(info.address)
        self.set_zn(self.x)

    # LDY - Load Y Register
    def ldy(self, info: StepInfo):
        self.y = self.memory.read(info.address)
        self.set_zn(self.y)

    # LSR - Logical Shift Right
    def lsr(self, info: StepInfo):
        if info.mode == MODE_ACCUMULATOR:
            self.c = self.a & 1
            self.a >>= 1
            self.set_zn(self.a)
        else:
            value = self.memory.read(info.address)
            self.c = value & 1
            value >>= 1
            print("lsr", info.address, value)
            self.memory.write(info.address, value)
            self.set_zn(value)

    # NOP - No Operation
    def nop(self, info: StepInfo):
        pass

    # ORA - Logical Inclusive OR
    def ora(self, info: StepInfo):
        self.a = self.a | self.memory.read(info.address)
        self.set_zn(self.a)

    # PHA - Push Accumulator
    def pha(self, info: StepInfo):
        self.push(self.a)

    # PHP - Push Processor Status
    def php(self, info: StepInfo):
        self.push(self.flags() | 0x10)

    # PLA - Pull Accumulator
    def pla(self, info: StepInfo):
        self.a = self.pull()
        print("pla", self.a)
        self.set_zn(self.a)

    # PLP - Pull Processor Status
    def plp(self, info: StepInfo):
        self.set_flags(self.pull() & 0xEF | 0x20)

    # ROL - Rotate Left
    def rol(self, info: StepInfo):
        if info.mode == MODE_ACCUMULATOR:
            c = self.c
            self.c = (self.a >> 7) & 1
            self.a = (self.a << 1) | c
            self.set_zn(self.a)
        else:
            c = self.c
            value = self.memory.read(info.address)
            self.c = (value >> 7) & 1
            value = (value << 1) | c
            print("rol", info.address, value)
            self.memory.write(info.address, value)
            self.set_zn(value)

    # ROR - Rotate Right
    def ror(self, info: StepInfo):
        if info.mode == MODE_ACCUMULATOR:
            c = self.c
            self.c = self.a & 1
            self.a = (self.a >> 1) | (c << 7)
            self.set_zn(self.a)
        else:
            c = self.c
            value = self.memory.read(info.address)
            self.c = value & 1
            value = (value >> 1) | (c << 7)
            print("ror", info.address, value)
            self.memory.write(info.address, value)
            self.set_zn(value)

    # RTI - Return from Interrupt
    def rti(self, info: StepInfo):
        self.set_flags(self.pull() & 0xEF | 0x20)
        self.pc = self.pull16()

    # RTS - Return form Subroutine
    def rts(self, info: StepInfo):
        self.pc = self.pull16() + 1

    # SBC - Subtract with Carry
    def sbc(self, info: StepInfo):
        a = int(self.a)
        b = int(self.memory.read(info.address))
        c = int(self.c)
        self.a = a - b - (1 - c)
        if self.a < 0:
            self.a += 256

        self.set_zn(self.a)
        if int(a) - int(b) - int(1-c) >= 0:
            self.c = 1
        else:
            self.c = 0
        if ((not (a ^ b) & 0x80) == 0) & ((not (a ^ self.a) & 0x80) == 0):
            self.v = 1
        else:
            self.v = 0

    # SEC - Set Carry Flag
    def sec(self, info: StepInfo):
        self.c = 1

    # SED - Set Decimal Flag
    def sed(self, info: StepInfo):
        self.d = 1

    # SEI - Set Interrupt Disable
    def sei(self, info: StepInfo):
        self.i = 1

    # STA - Store Accumulator
    def sta(self, info: StepInfo):
        print("sta", info.address, self.a)
        self.memory.write(info.address, self.a)

    # STX - Store X Register
    def stx(self, info: StepInfo):
        print("stx", info.address, self.x)
        self.memory.write(info.address, self.x)

    # STY - Store Y Register
    def sty(self, info: StepInfo):
        print("sty", info.address, self.y)
        self.memory.write(info.address, self.y)

    # TAX - Transfer Accumulator to X
    def tax(self, info: StepInfo):
        self.x = self.a
        self.set_zn(self.x)

    # TAY - Transfer Accumulator to Y
    def tay(self, info: StepInfo):
        self.y = self.a
        self.set_zn(self.y)

    # TSX - Transfer Stack Pointer to X
    def tsx(self, info: StepInfo):
        self.x = self.sp
        self.set_zn(self.x)

    # TXA - Transfer X to Accumulator
    def txa(self, info: StepInfo):
        self.a = self.x
        self.set_zn(self.a)

    # TXS - Transfer X to Stack Pointer
    def txs(self, info: StepInfo):
        self.sp = self.x

    # TYA - Transfer Y to Accumulator
    def tya(self, info: StepInfo):
        self.a = self.y
        self.set_zn(self.a)

    # illegal opcodes below
    def ahx(self, info: StepInfo):
        pass

    def alr(self, info: StepInfo):
        pass

    def anc(self, info: StepInfo):
        pass

    def arr(self, info: StepInfo):
        pass

    def axs(self, info: StepInfo):
        pass

    def dcp(self, info: StepInfo):
        pass

    def isc(self, info: StepInfo):
        pass

    def kil(self, info: StepInfo):
        pass

    def las(self, info: StepInfo):
        pass

    def lax(self, info: StepInfo):
        pass

    def rla(self, info: StepInfo):
        pass

    def rra(self, info: StepInfo):
        pass

    def sax(self, info: StepInfo):
        pass

    def shx(self, info: StepInfo):
        pass

    def shy(self, info: StepInfo):
        pass

    def slo(self, info: StepInfo):
        pass

    def sre(self, info: StepInfo):
        pass

    def tas(self, info: StepInfo):
        pass

    def xaa(self, info: StepInfo):
        pass


