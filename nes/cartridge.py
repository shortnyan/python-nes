# coding: utf-8


class Cartridge:
    def __init__(self, prg, chr, sram, mapper, mirror, battery):
        self.prg = prg
        self.chr = chr
        self.sram = sram
        self.mapper = mapper
        self.mirror = mirror
        self.battery = battery

    @classmethod
    def new_cartridge(cls, prg, chr, mapper, mirror, battery):
        sram = []
        return Cartridge(prg, chr, sram, mapper, mirror, battery)

    def save(self, encoder):
        encoder.encode(self.sram)
        encoder.encode(self.mirror)

    def load(self, decoder):
        self.sram = decoder.read()
        self.mirror = decoder.read()

