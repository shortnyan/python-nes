# coding: utf-8


def new_cpu_memory(console):
    return CpuMemory(console)


class Memory:
    def __init__(self, console):
        self.console = console


# CPU Memory Map
class CpuMemory(Memory):

    def read(self, address):
        if address < 0x2000:
            return self.console.ram[address % 0x0800]
        elif address < 0x4000:
            return self.console.ppu.read_register(0x2000 + (address % 8))
        elif address == 0x4014:
            return self.console.ppu.read_register(address)
        elif address == 0x4015:
            # TODO
            # return self.console.apu.read_register(address)
            pass
        elif address == 0x4016:
            return self.console.controller1.read()
        elif address == 0x4017:
            return self.console.controller2.read()
        elif address < 0x6000:
            # TODO I/O registers
            pass
        elif address >= 0x6000:
            return self.console.mapper.read(address)
        else:
            return 0

    def write(self, address, value):
        if address < 0x2000:
            self.console.ram[address % 0x0800] = value
        elif address < 0x4000:
            self.console.ppu.write_register(0x2000 + (address % 8), value)
        elif address < 0x4014:
            # TODO
            # self.console.apu.write_register(address, value)
            pass
        elif address == 0x4014:
            self.console.ppu.write_register(address, value)
        elif address == 0x4015:
            # TODO
            # self.console.apu.write_register(address, value)
            pass
        elif address == 0x4016:
            self.console.controller1.write(value)
            self.console.controller2.write(value)
        elif address == 0x4017:
            # TODO
            # self.console.apu.write_register(address, value)
            pass
        elif address < 0x6000:
            # TODO I/O registers
            pass
        elif address >= 0x6000:
            self.console.mapper.write(address, value)
        else:
            return 0


def new_ppu_memory(console):
    return PpuMemory(console)


# PPU Memory Map
class PpuMemory(Memory):
    def read(self, address):
        if address == 8192:
            print("ppu read 8192", address, address % 0x4000)
        address = address % 0x4000

        if address < 0x2000:
            return self.console.mapper.read(address)
        elif address < 0x3F00:
            mode = self.console.cartridge.mirror
            return self.console.ppu.name_table_data[mirror_address(mode, address) % 2048]
        elif address < 0x4000:
            return self.console.ppu.read_palette(address % 32)
        else:
            return 0

    def write(self, address, value):

        address = address % 0x4000
        if address < 0x2000:
            self.console.mapper.write(address, value)
        elif address < 0x3F00:
            mode = self.console.cartridge.mirror
            self.console.ppu.name_table_data[mirror_address(mode, address) % 2048] = value
        elif address < 0x4000:
            self.console.ppu.write_palette(address % 32, value)
        else:
            return 0

# Mirroring Modes
MIRROR_HORIZONTAL = 0
MIRROR_VERTICAL = 1
MIRROR_SINGLE0 = 2
MIRROR_SINGLE1 = 3
MIRROR_FOUR = 4

mirror_lookup = [
    [0, 0, 1, 1],
    [0, 1, 0, 1],
    [0, 0, 0, 0],
    [1, 1, 1, 1],
    [0, 1, 2, 3],
]


def mirror_address(mode, address):
    address = (address - 0x2000) % 0x1000
    table = int(address / 0x0400)
    offset = address % 0x0400
    return 0x2000 + mirror_lookup[mode][table] * 0x0400 + offset


