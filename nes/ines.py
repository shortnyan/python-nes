# coding: utf-8
import nes.cartridge


INES_FILE_MAGIC = b'NES'  # "0x1a53454e"


class INesFileHeader:
    def __init__(self, magic, num_prg, num_chr, control1, control2, num_ram, padding):
        self.magic = magic  # iNES magic number
        self.num_prg = num_prg  # number of PRG-ROM banks (16KB each)
        self.num_chr = num_chr  # number of CHR-ROM banks (8KB each)
        self.control1 = control1  # control bits
        self.control2 = control2  # control bits
        self.num_ram = num_ram  # PRG-RAM size (x 8KB)
        self._ = padding  # unused padding


#  load_nes_file reads an iNES file (.nes) and returns a Cartridge on success.
#  http://wiki.nesdev.com/w/index.php/INES
#  http://nesdev.com/NESDoc.pdf (page 28)
def load_nes_file(path):
    # open file
    file = open(path, 'rb')

    # read file header
    header_data = file.read(16)
    header = INesFileHeader(
        header_data[:3],
        header_data[4],
        header_data[5],
        header_data[6],
        header_data[7],
        header_data[8],
        header_data[9:16],
    )

    # print("header", header.control1)
    # print(vars(header))

    # verify header magic number
    if header.magic != INES_FILE_MAGIC:
        return None

    # mapper type
    mapper1 = header.control1 >> 4
    mapper2 = header.control2 >> 4
    mapper = mapper1 | mapper2 << 4

    # mirroring type
    mirror1 = header.control1 & 1
    mirror2 = (header.control1 >> 3) & 1
    mirror = mirror1 | mirror2 << 1

    # battery-backed ram
    battery = (header.control1 >> 1) & 1

    # read trainer if present (unused)
    if (header.control1 & 4) == 4:
        file.seek(512)

    # read prg-rom bank(s)
    prg_size = int(header.num_prg) * 16384
    prg = file.read(prg_size)

    # read chr-rom bank(s)
    chr_size = int(header.num_chr) * 8192
    chr = file.read(chr_size)

    # provide chr-rom/ram if not in file
    if header.num_chr == 0:
        chr = []

    # success
    return nes.cartridge.Cartridge.new_cartridge(prg, chr, mapper, mirror, battery)

