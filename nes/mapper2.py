# coding: utf-8

import nes.cartridge


class Mapper2:
    def __init__(self, cartridge: nes.cartridge.Cartridge):
        prg_banks = int(len(cartridge.prg) / 0x4000)
        self.cartridge = cartridge
        self.prg_banks = prg_banks
        self.prg_banks1 = 0
        self.prg_banks2 = prg_banks - 1

    def step(self):
        pass

    def read(self, address):
        if address < 0x2000:
            return self.cartridge.chr[address]
        elif address >= 0xc000:
            index = int(self.prg_banks2 * 0x4000 + address - 0xC000)
            return self.cartridge.prg[index]
        elif address >= 0x8000:
            index = int(self.prg_banks1 * 0x4000 + address - 0x8000)
            return self.cartridge.prg[index]
        elif address >= 0x6000:
            index = int(address - 0x6000)
            return self.cartridge.sram[index]
        else:
            return 0

    def write(self, address, value):
        if address < 0x2000:
            self.cartridge.chr[address] = value
        elif address >= 0x8000:
            self.prg_banks1 = int(value) % self.prg_banks
        elif address >= 0x6000:
            index = int(address - 0x6000)
            self.cartridge.sram[index] = value
