# coding: utf-8
import math


class FirstOrderFilter:
    def __init__(self, b0, b1, a1):
        self.b0 = b0
        self.b1 = b1
        self.a1 = a1
        self.prev_x = 0
        self.prev_y = 0

    def step(self, x):
        y = self.b0*x + self.b1*self.prev_x - self.a1*self.prev_y
        self.prev_y = y
        self.prev_x = x
        return y


def low_pass_filter(sample_rate, cut_off_freq):
    c = sample_rate / math.pi / cut_off_freq
    a0i = 1 / (1 + c)
    return FirstOrderFilter(
        a0i,
        a0i,
        (1-c) * a0i
    )


def high_pass_filter(sample_rate, cut_off_freq):
    c = sample_rate / math.pi / cut_off_freq
    a0i = 1 / (1 + c)
    return FirstOrderFilter(
        c * a0i,
        -c * a0i,
        (1 - c) * a0i
    )


def filter_chain(filter_list):
    x = 0
    if len(filter_list) != 0:
        for i, fc in filter_list:
            x = filter_list[i].step(x)
    return x


