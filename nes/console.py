# coding: utf-8

import os
import pickle
import nes.ines
import nes.controller
import nes.mapper
import nes.cpu
import nes.apu
import nes.ppu
import nes.filter
import nes.palette

class Console:
    def __init__(self, cpu, apu, ppu, cartridge, controller1, controller2, mapper, ram):
        self.cpu = cpu
        self.apu = apu
        self.ppu = ppu
        self.cartridge = cartridge
        self.controller1 = controller1
        self.controller2 = controller2
        self.mapper = mapper
        self.ram = ram

    @classmethod
    def new_console(cls, path):
        cartridge = nes.ines.load_nes_file(path)
        ram = [0 for _ in range(2048)]
        controller1 = nes.controller.Controller()
        controller2 = nes.controller.Controller()
        console = Console(None, None, None, cartridge, controller1, controller2, None, ram)
        console.mapper = nes.mapper.new_mapper(console)
        console.cpu = nes.cpu.CPU.new_cpu(console)
        # TODO
        console.apu = None # nes.apu.APU.new_apu(console)
        console.ppu = nes.ppu.PPU.new_ppu(console)
        return console

    def reset(self):
        self.cpu.reset()

    def step(self):
        cpu_cycles = self.cpu.step()
        ppu_cycles = cpu_cycles * 3

        for i in range(ppu_cycles):
            pass
            self.ppu.step()
            self.mapper.step()

        for i in range(cpu_cycles):
            # TODO
            # self.apu.step()
            pass

        return cpu_cycles

    """
    def step_frame(self):
        cpu_cycles = 0
        frame = self.ppu.frame
        while frame == self.ppu.frame:
            cpu_cycles += self.step()
        return cpu_cycles
    """

    def step_seconds(self, seconds):
        cycles = nes.cpu.CPU_FREQUENCY * seconds
        while cycles > 0:
            cycles -= self.step()
        # exit()

    def buffer(self):
        return self.ppu.front

    def background_color(self):
        return nes.palette.palette[self.ppu.read_palette(0) % 64]

    def set_buttons1(self, buttons):
        self.controller1.set_buttons(buttons)

    def set_buttons2(self, buttons):
        self.controller2.set_buttons(buttons)

    """
    def set_audio_channel(self, channel):
        self.apu.channel = channel

    def set_audio_sample_rate(self, sample_rate):
        if sample_rate != 0:
            self.apu.sample_rate = nes.cpu.CPU_FREQUENCY / sample_rate
            self.apu.filter_chain = nes.filter.filter_chain(
                nes.filter.high_pass_filter(float(sample_rate), 90),
                nes.filter.high_pass_filter(float(sample_rate), 440),
                nes.filter.high_pass_filter(float(sample_rate), 14000)
            )
        else:
            self.apu.filter_chain = None
    """

    def save_state(self, file_name):
        dir_name, _ = os.path.split(file_name)
        os.makedirs(dir_name)
        encoder = open(file_name, 'rb')
        # defer
        # encoder = gob.new_encoder(file)
        self.save(encoder)

    def save(self, encoder):
        # encoder.encode(self.ram)
        encoder.write(self.ram)
        self.cpu.save(encoder)
        self.apu.save(encoder)
        self.ppu.save(encoder)
        self.cartridge.save(encoder)
        self.mapper.save(encoder)
        # return encoder.encode(True)

    def load_state(self, filename):
        if not os.path.exists(filename):
            return
        file = open(filename, 'rb')
        self.load(file)

    def load(self, file):
        self.ram = file.read(2048)
        self.cpu.load(file)
        self.ppu.load(file)
        self.cartridge.load(file)
        self.mapper.load(file)
        # decoder.decode(dummy)

